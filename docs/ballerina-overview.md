Ballerina -- An overview
=========================

This document compiles a set of resources related the [Ballerina](https://ballerina.io) programming language.

# What is Ballerina?

The *Ballerina* programming language and its runtime stack were created to address the pain points developers faced while integrating independent components into a complex application. According to [[1]](https://www.infoq.com/articles/ballerina-microservices-language-part-1/), *Ballerina* makes it possible to write resilient programs that integrate and orchestrate across distributed endpoints. From a pure programming language perspective, it is a *compiled*, *statically* and *strongly* typed programming language.

# Installation

To install *Ballerina*, go to the [Download page](https://ballerina.io/downloads/) and follow the instructions according to your operating system. Once the installation completes, make sure it is added to your PATH environment variable. You can check the successful installation of *Ballerina* by printing its version `bal --version`. At the time of this write-up, the version is _Swan Lake Beta2_.

# Documentation and Resources

The main website of the language contains a lot of resources and documentation on the language. The key concepts and syntax are presented [here](https://ballerina.io/learn/language-concepts/). More resources include a [blog](https://blog.ballerina.io) and a collection of [examples](https://ballerina.io/learn/by-example/) for a variety of functions in *Ballerina*. You are strongly encouraged to start your journey by reading the documentation on the website. *Ballerina* also has a [Playground](https://play.ballerina.io) where you can test a piece of code online.

# References

<a id="1">[1]</a>
Jewel, T. (2018).
Ballerina Microservices Programming Language: Introducing the Latest Release and "Ballerina Central"
